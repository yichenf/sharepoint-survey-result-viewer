import React from 'react';
export function NumericAnswers(props) {
  let sum = 0;
  let count = 0;
  props.data
  .forEach(element => {
    if (!isNaN(element[props.field.EntityPropertyName]))
      sum = sum + element[props.field.EntityPropertyName];
    count++;
  });
  if (count > 0) {
    return (<div>
      <h4>{count} Users answered this question.</h4>
      <h4>Average: {sum / count}</h4>
    </div>);
  }
  else {
    return <div>No user answered this question</div>;
  }
}
