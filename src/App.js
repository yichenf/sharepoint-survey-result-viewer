import React, {useState} from 'react';
import Questions from './questions';
import URLController from './URLController';
import ResultViewer from './ResultViewer';
import UserStats from './UserStats';


function App() {
  const [apiUrl, seturl]=useState("");
  const [selectedfield, selectField]=useState(null);
  //using conditional rendering, to avoid initalization error due to null or empty props values
  return (
    <div className="App">
      <div className="sideBar">
        <URLController onChange={seturl}></URLController>
        {apiUrl!=="" && <Questions rooturl={apiUrl} onChange={selectField}></Questions>}      
      </div>
   
      <div className="main">
     
      {apiUrl!=="" && <UserStats rooturl={apiUrl}></UserStats>}      
        {selectedfield!==null &&
        <ResultViewer rooturl={apiUrl} field={selectedfield}></ResultViewer>}
      </div>      
      
    </div>
  );
}

export default App;

