import React from 'react';
import {authorDict, site} from './context';



//expandable component for datatables
export const AuthorList= ({data})=>{
    return (<div className={"authorList"}>
        {data.length>0? <h5>Voted by the following:</h5>:<h5>No users has selected this option</h5>}
        
        {data.length>0&&data.map(item=><Author item={item} key={item}/>)}
        </div>)}

export function Author(props){
return  <a style={{margin:'5px', fontWeight:400}} target="_blank" rel="noopener noreferrer" href={site.url+"/DispForm.aspx?ID="+props.item}>{Object.keys(authorDict).length>0?<span>{authorDict[props.item]["Title"]}</span>:<span>{props.item}}</span>}</a>

/**When author information is collected in the survey */
}

