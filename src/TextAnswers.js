import React from 'react';
import { Author } from './AuthorsWindow';
function TextAnswers(props) {
  const answers=props.data.filter(item=>{return item[props.field.EntityPropertyName]!=null})
  return (
    <div>
      <h3>{answers.length} User{answers.length>1 && "s"} has answered this question</h3>
      <ul>
        {answers.map(item=>
        <li>
<Author item={item.ID}/>: {item[props.field.EntityPropertyName]}
        </li>
          
          )}
      </ul>
    </div>  
);
}
export default TextAnswers;
