import React, {useState } from 'react';
import {AuthorList} from './AuthorsWindow';
import { VBar } from './PercentBar';
function RatingScale(props) {

  const [selectedCell,selectCell]=useState({row:-1, col:-1, value:[]});
  if (props.field.RangeCount) { 
    const data=props.data;
    const choices=props.field.Choices.results;
    
    //create an empty table
    const stats=choices.map((choice)=>{
      let row={"question":choice};
      for (let i=1; i<=props.field.RangeCount; i++){row[i]=[];}
      return row;
    });
    const titles=Array.from(Array(props.field.RangeCount),(_,i)=>i+1);
    //in case last label for titles is "N/A", values will be used to match answers' values
    const values=Array.from(Array(props.field.RangeCount),(_,i)=>i+1);
    if (props.field.RangeCount-props.field.GridEndNumber === 1){
      //check if there M/A option
      titles[titles.length-1]="N/A";
    }
    //iterate through results to fill the table with count of values
    let rscount=0;
    data.forEach(item=>{
  
      if(item[props.field.EntityPropertyName]!=null){
        //them for each of the choices, push item id into the stats table
        item[props.field.EntityPropertyName].results.forEach((res,i)=>{
          stats[i][res.Answer].push(item.ID);
        })
        rscount++;
      }
      
    });
    //get average for each row
    stats.forEach(row=>{
      let sum=0;
      let count=0;
      for (let i=props.field.GridStartNumber; i<=props.field.GridEndNumber;i++){
        sum+=row[i].length*i;
        count+=row[i].length;
      }
      row["avg"]=count>0?(sum/count).toFixed(1):0;
    })
    

    return (
      <div>
        <h3>{rscount} User{rscount>1&&"s"} answered this question</h3>
        {rscount>0&&
        <table>
          <thead>
          <tr><th>Choices</th>{titles.map(title=><th>{title}</th>)}
          <th>Average</th>
          </tr>
          
          </thead>
        <tbody>
        {choices.map((c,i)=>(
          <React.Fragment>
            <tr>
              <td>{c}</td>
              {values.map(s=>{
                let percent=(100*stats[i][s].length/rscount).toFixed(1);
                return <td onClick={()=>selectCell({row:i, col:s, value:stats[i][s]})} className={(selectedCell.row===i&&selectedCell.col===s)?"selected item":"item"}>
                  <VBar percent={percent}/>
                  <label>{stats[i][s].length}</label>
                  </td>}
              )} 
              <td>{stats[i].avg}</td>
            </tr>
            {(selectedCell.row === i )&&<tr >
              <td colSpan={props.field.RangeCount+1} className="selectedCell">
              <AuthorList data={selectedCell.value}></AuthorList>
              </td>
              
            </tr>}
          </React.Fragment>
        ))}

        </tbody>
          </table>
        }<div>Click corresponding number learn who voted for the choice</div>
      </div>
     
    )
  }
  else{
    return <div>You select question is not a Rating Scale</div>
  }
  
  
}
export default RatingScale;