import React from 'react';
import './vis.css';
export const VBar = ({ percent }) => <div className="vbarcontainer bar"><div className={"bar-space vbarblank"} style={{ height: (100-percent)+"%"  }}>{percent+"%"}</div></div>;
export const HBar = ({ percent }) => <div className="hbarcontainer bar"><div className={"bar-space hbarblank"} style={{ width: (100-percent)+"%" }}>{percent+"%"}</div></div>;
