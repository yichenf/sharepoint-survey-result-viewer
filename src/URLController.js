import React, {useState} from 'react';
import axios from 'axios';
import { site } from './context';
function URLController(props) {
  const [url, seturl] = useState("");
  const [sname, setName] = useState("");
  const [errorMessage,setErrorMessage]=useState("");
  const handleUrlChange = (e) => {
    seturl(e.currentTarget.value.trim());
  };
  const handleNameChange = (e) => {
    setName(e.currentTarget.value.trim());
  };
  const setSuerveyURL = (event) => {
    event.preventDefault();
    let tempurl=url;
    if (url.lastIndexOf("/_api/Web/Lists/") < 0 && url.lastIndexOf('/Lists/') > 0) {
      let uris = url.split("/");
      let i = uris.lastIndexOf("Lists");
      uris.splice(i + 2); //remove anything beyond list name
      site.url=uris.join("/");//save this as root of survey url
      let surveyname=sname.trim().length>0?encodeURI(sname.trim()):uris[i+1];
      uris.splice(i + 1, 1, "GetByTitle('" + surveyname + "')");
      uris.splice(i, 0, "_api", "Web");
      tempurl=uris.join("/");  
    }
    validateURL(tempurl);
  };

  const validateURL=(testurl)=>{
    axios.get(testurl).then(() => {
      props.onChange(testurl);
      console.log("rooturl",testurl);
      //set url to 
      //seturl(testurl);
      setErrorMessage("");
    }).catch(e => {
      console.log(e);
      setErrorMessage(testurl)
    });
  }

  return (<div style={{margin:'10px'}}>
    Please enter the details of your survey
       <div><label>URL:</label><input type='text' placeholder='e.g., https://nexus.icr.ac.uk/strategic-initiatives/sc/scientificsoftware/Lists/Software%20training%20survey/overview.aspx' value={url} onChange={handleUrlChange}></input></div>     
    <div><label>Survey Name:</label><input type='text' placeholder='e.g.Software survey' value={sname} onChange={handleNameChange}></input></div>
    <button onClick={setSuerveyURL}>Go</button>
    { errorMessage.trim().length>1 &&  
     <div style={{background:'rgba(200,0,0,0.2)',padding:'10px', margin:'5px 0px','border-radius':'5px'}}>invalid url, or you are not allowed to access <a href={errorMessage}>it</a></div>
    }   
  </div>);
}

export default URLController;
