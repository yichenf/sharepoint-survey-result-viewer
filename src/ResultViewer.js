import React,{useState, useEffect} from 'react';

import axios from 'axios';
import RatingScale from './RatingScale';
import TextAnswers from './TextAnswers';
import MultipleChoice from './MultipleChoice';
import { NumericAnswers } from './NumericAnswers';
import './ResultView.css';

function ResultViewer(props) {
    const [questionType, setQuestionType]=useState("");
    
    //@TODO: remember to remove testing data for results
    const [results, setResults]=useState([]);
    const supportedTypes=["MultiChoice","Choice","Boolean","GridChoice","Number"];
    const questionTypes=[{name: "MultiChoice", label:"Miltiple Choices (including Yes/No questions)"}, {name:"Note", label:"Text"}, {name:"Number", label:"Number"},{name:"other",label:"None of the above"}];
    useEffect(()=>{
      setQuestionType(props.field.TypeAsString);
      if (supportedTypes.lastIndexOf(props.field.TypeAsString)<0){
        console.log(props.field.TypeAsString+" may not be supported");
        setResults([]);
      }
      else{
        fetchResults()
        //test();
      }
      
    },[props.field]);
    function handleSelect(event){
      setQuestionType(event.currentTarget.value)
    }

    function fetchResults(){
       let url =props.rooturl+"/items?$select=ID,"+props.field.EntityPropertyName;
       let config= {
        headers: {
            "Accept": "application/json;odata=verbose",
            "Content-Type":"application/json;odata=verbose"
        }   
    };    
       axios.get(url,config).then(response => {
         let data=response.data;
        if(data.hasOwnProperty("d")&& data.d.hasOwnProperty("results")){
          setResults(data.d.results);
        }
        else{
          console.log(data);
          setResults([]);
          alert ("An Error occured!");
        }
       
      }).catch(e => {
        console.log("when fetching results:",e)
      })    
      
    }
    return (
        <div>
            <h4><b>Question:</b> {props.field.Title}</h4>
            {supportedTypes.lastIndexOf(props.field.TypeAsString)<0&&<div><label>Question Type: 
            <select value={questionType} onChange={handleSelect}>
                {questionTypes.map(q=>{
                    return <option value={q.name} onChange={()=>setQuestionType(q.name)}>{q.label}</option>
                })} 
            </select></label>
            <button onClick={(event)=>{event.preventDefault();fetchResults()}}>Go</button>
            </div>}
            {results.length>0&&<div>
            { (questionType==="MultiChoice" || questionType==="Choice" || questionType==="Boolean") && <MultipleChoice data={results} field={props.field}></MultipleChoice>}
            { questionType === "GridChoice" && <RatingScale data={results} field={props.field} ></RatingScale>}
            { questionType === "Note" && <TextAnswers data={results} field={props.field}></TextAnswers>}
            { questionType === "Number" && <NumericAnswers data={results} field={props.field}></NumericAnswers>}
            { questionType === "other" && <div>Sorry, Currently supported question types only include the following:
              <ul>
                {["Mutiple Choice(s)", "Rating scale", "Number","Text"].map(q=><li>{q}</li>)}
                </ul>. <p>Please use Sharepoint's default view to see the statistics</p></div>}
            </div>
            }
            </div> 
    )
;
    
}
export default ResultViewer;