import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import {authorDict} from './context'
function UserStats(props) {
  //creates a authoritem table

  const [responseCount, setResponseCount] = useState(0);
  function processData( data){
    if(data.d.results){
        //authorDict={};
        data.d.results.forEach(res=> {
            authorDict[res.ID]=res["Author"];
        });
        setResponseCount(data.d.results.length);
    }
}
  useEffect(()=>{
    if (props.rooturl!=="")
     {
       let authorurl=props.rooturl+"/items?$select=ID,Author/ID,Author/Title,Author/SipAddress,Author/Department&$expand=Author";
       let config= {
        headers: {
            "Accept": "application/json;odata=verbose",
            "Content-Type":"application/json;odata=verbose"
        }   
    };
       Axios.get(authorurl,config).then(
      response=>{
        let data=response.data;
          processData(data)}
    ).catch(
        e=>{
          console.log(e)
        }
    );}
    }, [props.rooturl]);

  return <div>
    <h4>There are a total of <b>{responseCount}</b> response{responseCount > 1 && "s"}</h4>
  </div>;
}
export default UserStats;