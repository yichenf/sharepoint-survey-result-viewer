import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';
import {AuthorList} from './AuthorsWindow';
import { HBar } from './PercentBar';
function MultipleChoice(props) {
  const [totalAnswers, setTotalAnswers]=useState(0);
  const [stats, setStats]=useState([]);

  useEffect(()=>{
      let items=props.data;
      let field=props.field.EntityPropertyName;
      let choices={};
      let answersCount=0;
      items.forEach(item=>{
        if (item[field]!=null){
          answersCount++;
          let values=item[field].hasOwnProperty("results")?item[field].results:item[field].toString().split(";");
        //count each choice;
          values.forEach(v=>{
            if (v.trim().length>0){
              if (choices.hasOwnProperty(v)){
                choices[v].count++;
                choices[v].authors.push(item.ID);
              }
              else{choices[v]={count:1, authors:[item.ID]};}
            }});
        }
        setTotalAnswers(answersCount);
        if(answersCount>0){
          
          let data=Object.entries(choices).map(([key, value])=>{
            return {choice:key, count:value.count, percent: (100*value.count/answersCount).toFixed(1), authors:value.authors};
          });
          setStats(data);
        }
      });
    
  },[props.data, props.field])
  return (
    <div>
      <h3>{totalAnswers} User{totalAnswers>1&&"s"} answered this question</h3>
      <DataTable 
      data={stats}
      columns={columns} 
      expandableRows
      expandableRowsComponent={<DataTableExpandableComponent/>}
      ></DataTable>
    </div>  
);
}
const DataTableExpandableComponent=({data})=><AuthorList data={data.authors}></AuthorList>
const columns = [
  {
    name: 'Choice',
    selector: 'choice',
    sortable: true,
  },
  {
    name: 'Number of Users',
    selector: 'count',
    sortable: true,
  },
  {
    name: 'Percent of users (%)', 
    selector:'percent',
    cell: row => <HBar percent={row.percent}/>,
  }
];
export default MultipleChoice;
