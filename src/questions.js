
import React,{useState, useEffect} from 'react';
import axios from 'axios';
//import {questionlist} from './testdata';
 function Questions(props){
    const [questions, setQuestions]=useState([]);
    const [selectedQuestion,setSelectedQuestion]=useState({EntityPropertyName:""});

    function selectQuestion (q) {
        setSelectedQuestion(q);
        props.onChange(q);
    }
    useEffect(
        () => {
            //fetches the list of questions from sharepoint API
            let config= {
                headers: {
                    "Accept": "application/json;odata=verbose",
                    "Content-Type":"application/json;odata=verbose"
                }   
            };
            if (props.rooturl!==""){
                let url=props.rooturl+"/Fields?$select=EntityPropertyName,Title,TypeDisplayName,TypeAsString,Choices,GridStartNumber,GridEndNumber,RangeCount&$filter=(CanBeDeleted%20eq%20true)";
                axios.get(url,config)
                .then(response=>{
                    let data=response.data;
                    //process response
                    //setList
                    setQuestions(data.d.results);
                    
                })
                .catch(e=>{
                    console.log(e);
                }) 
            }   
        }
        ,[props.rooturl]);
    function trimedStr(str){
        //lets limit the length titles to 50 characters
        let maxlen=50;  
        str=str.trim();
        return str.length<=maxlen?  str: str.substr(0,str.indexOf(" ", maxlen-1)>0?str.indexOf(" ", maxlen-1):str.lastIndexOf(' '))+"...";
    }
    return (
        
        <div>
            <h3>Please select a question</h3>
                {questions.length>0 &&
            questions.map(q=>{
                return <div className={selectedQuestion.EntityPropertyName === q.EntityPropertyName? "item selected":"item"} 
                onClick={()=>{selectQuestion (q)}}>
                    {trimedStr(q.Title)}
                </div>
            })}
        
        </div>
  
    )
    
}





export default Questions;