# Sharepoint Survey Result Viewer

This application provides a simple solutions for visualizing some of Sharepoint survey's results:

- multiple choices questions: allow users to see the count of each option, instead of taking every combinations of options as a new option


- enable uses to see who or each responses contributed to each answer or option

It currently support the following types of questions:
- multiple choices (checkbox)
- multiple choice(radio buttons, list, etc.)
- text (single line or multiple lines)
- numbers
- rating scales 

Please let us know if you'd like it to support other types of questions 