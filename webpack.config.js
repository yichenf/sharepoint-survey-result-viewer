const webpack = require('webpack');
const path = require('path');
//const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');;

const config = {
  entry: {
    app:['./src/index.js'],
  },
  output: {
  path: path.resolve(__dirname, 'static'),
  filename: '[name].min.js'
},
  
	resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
    },
  	module: {
		rules: [
			{
				test: /\.(js|jsx|tsx|ts)$/,
				exclude:path.resolve(__dirname, 'node_modules'),
				use: {
				loader: 'babel-loader',
				options: {
					presets: [
							'@babel/preset-env',
							'@babel/preset-react',
							'@babel/preset-typescript'
						],
						plugins : [
							["@babel/plugin-proposal-decorators", { "legacy": true }],
							'@babel/plugin-syntax-dynamic-import',
							['@babel/plugin-proposal-class-properties', { "loose": true }]
						]
					}
				},
			},

			{
				test : /\.css$/,
				use: ['style-loader', 'css-loader']
			},
			
		]
	},
  plugins: [
		//new ExtractTextPlugin(path.join('..', 'css', 'app.css')),
		
	],
  
  };

if (process.env.NODE_ENV === 'production') {
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
    sourceMap: true,
    compress: {
      sequences: true,
      conditionals: true,
      booleans: true,
      if_return: true,
      join_vars: true,
      drop_console: true
    },
    output: {
      comments: false
    },
    minimize: true
    })
  );
}
else{
config["mode"]='development';
config["devtool"]='inline-source-map';
config.plugins.push(
  new CleanWebpackPlugin(),
  new HtmlWebpackPlugin({
  title: 'Development'
}))
}

module.exports = config;
